package com.cowbell.cordova.geofence;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;



public class GeofenceTransitionEnqueuer extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.setLogger(new Logger(GeofencePlugin.TAG, context, false));
        Logger logger = Logger.getLogger();
        logger.log(Log.DEBUG, "GeofenceTransitionEnqueuer - Broadcast GeofenceIntentReceived");
        intent.setClass(context, ReceiveTransitionsIntentService.class);
        ReceiveTransitionsIntentService.enqueueWork(context, intent);
        logger.log(Log.DEBUG, "GeofenceTransitionEnqueuer - Geofence Work enqueued");
    }
}
