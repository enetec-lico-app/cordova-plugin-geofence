package com.cowbell.cordova.geofence;

import com.google.android.gms.location.Geofence;
import com.google.gson.annotations.Expose;

public class GeoNotification {
    @Expose public String id;
    @Expose public double lat;
    @Expose public double lon;
    @Expose public int radius;
    @Expose public int transitionType;

    @Expose public Notification notification;

    public GeoNotification() {
    }

    public Geofence toGeofence() {
        return new Geofence.Builder()
            .setRequestId(id)
            .setTransitionTypes(3)
            .setCircularRegion(lat, lon, 500)
            .setExpirationDuration(Long.MAX_VALUE).build();
    }

    public String toJson() {
        return MyGson.get().toJson(this);
    }

    public static GeoNotification fromJson(String json) {
        if (json == null) return null;
        return MyGson.get().fromJson(json, GeoNotification.class);
    }
}
