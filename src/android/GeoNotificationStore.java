package com.cowbell.cordova.geofence;
import android.content.Context;
import com.google.gson.Gson;
import java.util.ArrayList;
import android.content.SharedPreferences;
import com.google.gson.reflect.TypeToken;
import java.util.List;

public class GeoNotificationStore {
    private LocalStorage storage;
    private Context context;

    public GeoNotificationStore(Context context) {
        this.context = context;
        storage = new LocalStorage(context);
    }

    public void setGeoNotification(GeoNotification geoNotification) {
        storage.setItem(geoNotification.id, MyGson.get().toJson(geoNotification));
    }

    public GeoNotification getGeoNotification(String id) {
        String objectJson = storage.getItem(id);
        return GeoNotification.fromJson(objectJson);
    }

    /* public List<GeoNotification> getAll() {
        List<String> objectJsonList = storage.getAllItems();
        List<GeoNotification> result = new ArrayList<GeoNotification>();
        for (String json : objectJsonList) {
            result.add(GeoNotification.fromJson(json));
        }
        return result;
    } */

    public void remove(String id) {
        storage.removeItem(id);
    }

    public void clear() {
        storage.clear();
    }

    public List<GeoNotification> getAll() {
        return new Gson().fromJson(context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE).getString("points", "[]"), new TypeToken<ArrayList<GeoNotification>>(){}.getType());
    }
}
