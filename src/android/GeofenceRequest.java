package com.cowbell.cordova.geofence;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonObject;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public class GeofenceRequest extends AsyncTask<Void, Void, Void> {

    private HttpsURLConnection urlConnection;
    private String authToken;
    private String id;
    private String action;

    public GeofenceRequest(String authToken, String id, String action){
        this.authToken = authToken;
        this.id = id;
        this.action = action;
    }

    @Override
    protected Void doInBackground(Void... args) {
        try {
            JsonObject params = new JsonObject();
            params.addProperty("action", action);

            URL url = new URL("https://geofence.sc365.enetec.info/v1/cross/" + id);
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", authToken);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
            outputStream.writeBytes(params.toString());
            outputStream.flush();
            outputStream.close();

            Log.d("Response", String.valueOf(urlConnection.getResponseCode()));
            Log.d("Message", urlConnection.getResponseMessage());


        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            urlConnection.disconnect();
        }
        return null;
    }
}
